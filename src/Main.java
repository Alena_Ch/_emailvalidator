import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {


    public static void main(String[] args) {
        System.out.println("Введите Ваш email: ");
        Scanner sc = new Scanner(System.in);
        String email = "alena1.chu@gmail.com";
        sc.nextLine();
        sc.close();

        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(email);


        boolean found = matcher.matches();

        if (found) {
            System.out.println("Введенный Вами email соответствует стандарту сети");
        } else {
            System.out.println("Проверьте правильность ввода данных!");
        }
    }
}
